﻿using ProjectTrackerWPF.ViewModels;
using System.Windows.Controls;

namespace ProjectTrackerWPF.Views
{
    /// <summary>
    /// Interaction logic for Osobe.xaml
    /// </summary>
    public partial class OsobeView : UserControl
    {
        public OsobeView()
        {
            InitializeComponent();
            DataContext = new OsobeViewModel();
        }
    }
}