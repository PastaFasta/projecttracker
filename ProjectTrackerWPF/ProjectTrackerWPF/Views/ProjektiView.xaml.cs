﻿using ProjectTrackerWPF.ViewModels;
using System.Windows.Controls;

namespace ProjectTrackerWPF.Views
{
    /// <summary>
    /// Interaction logic for ProjektiView.xaml
    /// </summary>
    public partial class ProjektiView : UserControl
    {
        public ProjektiView()
        {
            InitializeComponent();
            DataContext = new ProjektiViewModel();
        }

        private void DodajClick(object sender, System.Windows.RoutedEventArgs e)
        {
            this.contentControlResursi.Content = new DodavanjeResursaView();
        }
    }
}