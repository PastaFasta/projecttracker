﻿using ProjectTrackerWPF.ViewModels;
using System.Windows.Controls;

namespace ProjectTrackerWPF.Views
{
    /// <summary>
    /// Interaction logic for UlogeView.xaml
    /// </summary>
    public partial class UlogeView : UserControl
    {
        public UlogeView()
        {
            InitializeComponent();
            DataContext = new UlogeViewModel();
        }
    }
}