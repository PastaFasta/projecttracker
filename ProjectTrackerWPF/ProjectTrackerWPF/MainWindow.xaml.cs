﻿using ProjectTrackerWPF.Views;
using System.Windows;
using System.Windows.Controls;

namespace ProjectTrackerWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static ContentControl ContControl { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            ContControl = this.contentControl;
        }

        private void ClickOsobe(object sender, RoutedEventArgs e)
        {
            if (!(ContControl.Content is OsobeView))
            {
                ContControl.Content = new OsobeView();
            }
        }

        private void ClickProjekti(object sender, RoutedEventArgs e)
        {
            if (!(ContControl.Content is ProjektiView))
            {
                ContControl.Content = new ProjektiView();
            }
        }

        private void ClickUloge(object sender, RoutedEventArgs e)
        {
            if (!(ContControl.Content is UlogeView))
            {
                ContControl.Content = new UlogeView();
            }
        }
    }
}