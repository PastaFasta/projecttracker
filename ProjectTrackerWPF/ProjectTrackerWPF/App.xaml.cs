﻿using System.Windows;

namespace ProjectTrackerWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            ProjectTracker.Dal.ProjektFERContext.Instance.Initialize();
            //AutoMapperConfig.Configure();
            //ApplicationParameters.Initialize();
        }
    }
}