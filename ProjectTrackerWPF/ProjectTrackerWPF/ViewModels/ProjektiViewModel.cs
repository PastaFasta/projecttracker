﻿using Microsoft.VisualStudio.PlatformUI;
using ProjectTracker.BusinessLibrary.Core.Models;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace ProjectTrackerWPF.ViewModels
{
    public class ProjektiViewModel : INotifyPropertyChanged
    {
        private bool _novi;
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public DateTime? Zapocet { get; set; }
        public DateTime? Zavrsen { get; set; }

        public bool MozeSpremit { get; set; } = false;
        public bool IsRead { get; set; } = true;

        private ICommand _urediProjektCommand;
        private ICommand _dodajProjektCommand;
        private ICommand _spremiProjektCommand;
        private ICommand _dodajResursCommand;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyRaised(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }

        public ICommand UrediProjektCommand
        {
            get { return _urediProjektCommand ?? (_urediProjektCommand = new DelegateCommand(UrediProjekt)); }
        }

        public ICommand NoviProjektCommand
        {
            get { return _dodajProjektCommand ?? (_dodajProjektCommand = new DelegateCommand(DodajProjekt)); }
        }

        public ICommand SpremiProjektCommand
        {
            get { return _spremiProjektCommand ?? (_spremiProjektCommand = new DelegateCommand(SpremiProjekt)); }
        }

        public ICommand DodajResursCommand
        {
            get { return _dodajResursCommand ?? (_dodajResursCommand = new DelegateCommand(DodajResurs)); }
        }

        public ProjektInfoList ListaProjekata { get; set; } = ProjektInfoList.Get();
        public ProjektInfo SelektiraniProjekt { get; set; }
        public ProjektOsobaUlogaLists ListaResurasa { get; set; }

        public ProjektiViewModel()
        {
        }

        private void DodajProjekt()
        {
            _novi = true;
            Naziv = String.Empty;
            Opis = String.Empty;
            Zapocet = DateTime.Today;
            Zavrsen = DateTime.Today;
            IsRead = false;
            MozeSpremit = true;
            OnPropertyRaised("Naziv");
            OnPropertyRaised("Opis");
            OnPropertyRaised("Zapocet");
            OnPropertyRaised("Zavrsen");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsRead");
        }

        private void SpremiProjekt()
        {
            OnPropertyRaised("Naziv");
            OnPropertyRaised("Opis");
            OnPropertyRaised("Zapocet");
            OnPropertyRaised("Zavrsen");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsRead");
        }

        private void DodajResurs()
        {
        }

        private void UrediProjekt()
        {
            _novi = false;
            Naziv = SelektiraniProjekt.Naziv;
            Opis = SelektiraniProjekt.Opis;
            Zapocet = SelektiraniProjekt.Zapocet;
            Zavrsen = SelektiraniProjekt.Zavrsen;
            ListaResurasa = ProjektOsobaUlogaLists.Get(SelektiraniProjekt.Id);
            IsRead = false;
            MozeSpremit = true;
            OnPropertyRaised("Naziv");
            OnPropertyRaised("Opis");
            OnPropertyRaised("Zapocet");
            OnPropertyRaised("Zavrsen");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsRead");
            OnPropertyRaised("ListaResurasa");
            OnPropertyRaised("SelektiraniProjekt");
        }
    }
}