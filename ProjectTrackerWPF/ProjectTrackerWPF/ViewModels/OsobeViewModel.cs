﻿using Microsoft.VisualStudio.PlatformUI;
using ProjectTracker.BusinessLibrary.Core.Models;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace ProjectTrackerWPF.ViewModels
{
    public class OsobeViewModel : INotifyPropertyChanged
    {
        private bool _novi;
        private string _ime;
        private string _prezime;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyRaised(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }

        private ICommand _newOsobaCommand;
        private ICommand _editOsobaCommand;
        private ICommand _saveOsobaCommand;

        public ICommand NovaOsobaCommand
        {
            get { return _newOsobaCommand ?? (_newOsobaCommand = new DelegateCommand(DodajOsobu)); }
        }

        public ICommand EditOsobaCommand
        {
            get { return _editOsobaCommand ?? (_editOsobaCommand = new DelegateCommand(UrediOsobu)); }
        }

        public ICommand SpremiOsobuCommand
        {
            get { return _saveOsobaCommand ?? (_saveOsobaCommand = new DelegateCommand(SpremiOsobu)); }
        }

        public string Ime
        {
            get { return _ime; }
            set { _ime = value; }
        }

        public string Prezime
        {
            get { return _prezime; }
            set { _prezime = value; }
        }

        public string PunoIme
        {
            get { return Ime + " " + Prezime; }
        }

        public bool MozeSpremit { get; set; } = false;
        public bool IsReadOsoba { get; set; } = true;

        public OsobaInfoList ListaOsoba { get; set; } = OsobaInfoList.Get();

        public OsobaInfo SelektiranaOsoba { get; set; }

        public OsobeViewModel()
        {
        }

        private void DodajOsobu()
        {
            _novi = true;
            Ime = String.Empty;
            Prezime = String.Empty;
            IsReadOsoba = false;
            MozeSpremit = true;
            OnPropertyRaised("Ime");
            OnPropertyRaised("Prezime");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadOsoba");
        }

        private void UrediOsobu()
        {
            _novi = false;
            Ime = SelektiranaOsoba.Ime;
            Prezime = SelektiranaOsoba.Prezime;
            IsReadOsoba = false;
            MozeSpremit = true;
            OnPropertyRaised("Ime");
            OnPropertyRaised("Prezime");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadOsoba");
        }

        private void SpremiOsobu()
        {
            Osoba osoba;
            if (_novi)
            {
                osoba = Osoba.New();
                osoba.Ime = Ime;
                osoba.Prezime = Prezime;
                Osoba o = osoba.Save();
            }
            else
            {
                osoba = Osoba.Get(SelektiranaOsoba.Id);
                osoba.Ime = Ime;
                osoba.Prezime = Prezime;
                Osoba.Update(osoba);
            }
            MozeSpremit = false;
            IsReadOsoba = true;
            OnPropertyRaised("Ime");
            OnPropertyRaised("Prezime");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadOsoba");
            Ime = Prezime = String.Empty;
            ListaOsoba = OsobaInfoList.Get();
            OnPropertyRaised("ListaOsoba");
        }
    }
}