﻿using Microsoft.VisualStudio.PlatformUI;
using ProjectTracker.BusinessLibrary.Core.Models;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace ProjectTrackerWPF.ViewModels
{
    public class UlogeViewModel : INotifyPropertyChanged
    {
        private bool _novi;
        private ICommand _dodajUloguCommand;
        private ICommand _urediUloguCommand;
        private ICommand _spremiUloguCommand;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyRaised(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }

        public ICommand UrediUloguCommand
        {
            get { return _urediUloguCommand ?? (_urediUloguCommand = new DelegateCommand(UrediUlogu)); }
        }

        public ICommand NovaUlogaCommand
        {
            get { return _dodajUloguCommand ?? (_dodajUloguCommand = new DelegateCommand(DodajUlogu)); }
        }

        public ICommand SpremiUloguCommand
        {
            get { return _spremiUloguCommand ?? (_spremiUloguCommand = new DelegateCommand(SpremiUlogu)); }
        }

        public string Naziv { get; set; } = String.Empty;
        public bool MozeSpremit { get; set; } = false;
        public bool IsReadUloga { get; set; } = true;
        public UlogaInfoList ListaUloga { get; set; } = UlogaInfoList.Get();

        public UlogaInfo SelektiranaUloga { get; set; }

        public UlogeViewModel()
        {
        }

        private void UrediUlogu()
        {
            _novi = false;
            Naziv = SelektiranaUloga.Naziv;
            MozeSpremit = true;
            IsReadUloga = false;
            OnPropertyRaised("Naziv");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadUloga");
        }

        private void DodajUlogu()
        {
            _novi = true;
            Naziv = String.Empty;
            MozeSpremit = true;
            IsReadUloga = false;
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadUloga");
        }

        private void SpremiUlogu()
        {
            Uloga uloga;
            if (_novi)
            {
                uloga = Uloga.New();
                uloga.Naziv = Naziv;
                Uloga u = uloga.Save();
            }
            else
            {
                uloga = Uloga.Get(SelektiranaUloga.Id);
                uloga.Naziv = Naziv;
                Uloga.Update(uloga);
            }

            MozeSpremit = false;
            IsReadUloga = true;
            OnPropertyRaised("Naziv");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadUloga");
            var bu = uloga.GetBrokenRules();
            Naziv = String.Empty;
            ListaUloga = UlogaInfoList.Get();
            OnPropertyRaised("ListaUloga");
        }
    }
}