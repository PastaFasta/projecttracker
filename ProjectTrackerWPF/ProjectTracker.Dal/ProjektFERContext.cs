﻿using Microsoft.EntityFrameworkCore;
using ProjectTracker.Dal.DatabaseModels;

namespace ProjectTracker.Dal
{
    public partial class ProjektFERContext : DbContext
    {
        private static ProjektFERContext _instance;
        private static readonly object _lock = new object();

        public static ProjektFERContext Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = new ProjektFERContext();
                        }
                    }
                }

                return _instance;
            }
        }

        public ProjektFERContext()
        {
            Osoba.Load();
            Projekt.Load();
            Uloga.Load();
            ProjektUlogaOsoba.Load();
        }

        public ProjektFERContext(DbContextOptions<ProjektFERContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Osoba> Osoba { get; set; }
        public virtual DbSet<Projekt> Projekt { get; set; }
        public virtual DbSet<ProjektUlogaOsoba> ProjektUlogaOsoba { get; set; }
        public virtual DbSet<Uloga> Uloga { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=projektzafer2019m.database.windows.net;Initial Catalog=PPP20;User ID=ProjectTracker;Password=FERprojekt1");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Osoba>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Ime)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Prezime)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Projekt>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Naziv)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Opis)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Zapocet).HasColumnType("datetime");

                entity.Property(e => e.Zavrsen).HasColumnType("datetime");
            });

            modelBuilder.Entity<ProjektUlogaOsoba>(entity =>
            {
                entity.HasKey(e => new { e.IdProjekt, e.IdOsoba })
                    .HasName("PK__ProjektU__4A004E19D3C35EC0");

                entity.Property(e => e.IdProjekt).HasColumnName("ID_Projekt");

                entity.Property(e => e.IdOsoba).HasColumnName("ID_Osoba");

                entity.Property(e => e.IdUloga).HasColumnName("ID_Uloga");

                entity.HasOne(d => d.IdOsobaNavigation)
                    .WithMany(p => p.ProjektUlogaOsoba)
                    .HasForeignKey(d => d.IdOsoba)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ProjektUl__ID_Os__6477ECF3");

                entity.HasOne(d => d.IdProjektNavigation)
                    .WithMany(p => p.ProjektUlogaOsoba)
                    .HasForeignKey(d => d.IdProjekt)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ProjektUl__ID_Pr__628FA481");

                entity.HasOne(d => d.IdUlogaNavigation)
                    .WithMany(p => p.ProjektUlogaOsoba)
                    .HasForeignKey(d => d.IdUloga)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ProjektUl__ID_Ul__6383C8BA");
            });

            modelBuilder.Entity<Uloga>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Naziv)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public void Initialize()
        {
        }
    }
}