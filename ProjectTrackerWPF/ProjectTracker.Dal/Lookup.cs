﻿using ProjectTracker.Dal.DatabaseModels;
using System.Collections.Generic;
using System.Linq;
using System;

namespace ProjectTracker.Dal
{
    public static class Lookup
    {
        public static List<Osoba> FetchOsobe()
        {
            return ProjektFERContext.Instance.Osoba.ToList();
        }

        public static Osoba FetchOsoba(int id)
        {
            return ProjektFERContext.Instance.Osoba.Where(o => o.Id == id).FirstOrDefault();
        }

        public static List<Projekt> FetchProjekte()
        {
            return ProjektFERContext.Instance.Projekt.ToList();
        }
        public static Projekt FetchProjekt(int id) 
        {
            return ProjektFERContext.Instance.Projekt.Where(p => p.Id == id).FirstOrDefault();
        }

        public static List<Uloga> FetchUloge()
        {
            return ProjektFERContext.Instance.Uloga.ToList();
        }

        public static Uloga FetchUloga(int id)
        {
            return ProjektFERContext.Instance.Uloga.Where(u => u.Id == id).FirstOrDefault();
        }

        public static List<ProjektUlogaOsoba> FetchProjektUlogaOsoba()
        {
            return ProjektFERContext.Instance.ProjektUlogaOsoba.ToList();
        }

        public static List<Osoba> GetResursiFromProjekt(int projektID)
        {
            List<int> IDs = ProjektFERContext.Instance.ProjektUlogaOsoba.Where(p => p.IdProjekt == projektID).Select(p => p.IdOsoba).ToList();

            return ProjektFERContext.Instance.Osoba.Where(o => IDs.Contains(o.Id)).ToList();
        }

        public static List<Osoba> GetSlobodniResursi()
        {
            List<int> IDs = ProjektFERContext.Instance.ProjektUlogaOsoba.Where(p => p.IdProjekt == 0).Select(p => p.IdOsoba).ToList();

            return ProjektFERContext.Instance.Osoba.Where(o => IDs.Contains(o.Id)).ToList();
        }

        public static void AddProjekt(string naziv, string opis, DateTime? zapocet, DateTime? zavrsen)
        {
            Projekt projekt = new Projekt();
            projekt.Naziv = naziv;
            projekt.Opis = opis;
            projekt.Zapocet = zapocet;
            projekt.Zavrsen = zavrsen;
            ProjektFERContext.Instance.Projekt.Add(projekt);
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void AddOsoba(string ime, string prezime)
        {
            Osoba osoba = new Osoba();
            osoba.Ime = ime;
            osoba.Prezime = prezime;
            ProjektFERContext.Instance.Osoba.Add(osoba);
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void AddUloga(string naziv)
        {
            Uloga uloga = new Uloga();
            uloga.Naziv = naziv;
            ProjektFERContext.Instance.Uloga.Add(uloga);
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void AssignOsobaToProjekt(Osoba osoba, Projekt projekt, Uloga uloga)
        {
            ProjektFERContext.Instance.ProjektUlogaOsoba.Add(new ProjektUlogaOsoba() { IdOsoba = osoba.Id, IdProjekt = projekt.Id, IdUloga = uloga.Id });
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void RemoveOsoba(int id)
        {
            Osoba osoba = ProjektFERContext.Instance.Osoba.Where(o => o.Id == id).FirstOrDefault();
            ProjektFERContext.Instance.Osoba.Remove(osoba);
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void RemoveProjekt(int id)
        {
            Projekt projekt = ProjektFERContext.Instance.Projekt.Where(p => p.Id == id).FirstOrDefault();
            ProjektFERContext.Instance.Projekt.Remove(projekt);
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void RemoveUloga(int id)
        {
            Uloga uloga = ProjektFERContext.Instance.Uloga.Where(p => p.Id == id).FirstOrDefault();
            ProjektFERContext.Instance.Uloga.Remove(uloga);
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void UnassignOsobaFromObject(Osoba osoba, Projekt projekt)
        {
            ProjektUlogaOsoba projektUlogaOsoba = ProjektFERContext.Instance.ProjektUlogaOsoba.Where(p => p.IdOsoba == osoba.Id && p.IdProjekt == projekt.Id).FirstOrDefault();
            if (projektUlogaOsoba != null)
            {
                ProjektFERContext.Instance.ProjektUlogaOsoba.Remove(projektUlogaOsoba);
            }
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void UpdateProjekt(int id, string naziv, string opis, DateTime? zapocet, DateTime? zavrsen) {
            Projekt projekt = Lookup.FetchProjekt(id);
            projekt.Naziv = naziv;
            projekt.Opis = opis;
            projekt.Zapocet = zapocet;
            projekt.Zavrsen = zavrsen;
            ProjektFERContext.Instance.Projekt.Update(projekt);
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void UpdateOsoba(int id, string ime, string prezime)
        {
            Osoba osoba = Lookup.FetchOsoba(id);
            osoba.Ime = ime;
            osoba.Prezime = prezime;
            ProjektFERContext.Instance.Osoba.Update(osoba);
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void UpdateUloga(int id, string naziv)
        {
            Uloga uloga = Lookup.FetchUloga(id);
            uloga.Naziv = naziv;
            ProjektFERContext.Instance.Uloga.Update(uloga);
            ProjektFERContext.Instance.SaveChanges();
        }

        public static void UpdateUlogaToOsobaOnProject(Uloga uloga, Osoba osoba, Projekt projekt)
        {
            ProjektUlogaOsoba projektUlogaOsoba = ProjektFERContext.Instance.ProjektUlogaOsoba.Where(p => p.IdOsoba == osoba.Id && p.IdProjekt == projekt.Id).FirstOrDefault();
            projektUlogaOsoba.IdUloga = uloga.Id;
            ProjektFERContext.Instance.ProjektUlogaOsoba.Update(projektUlogaOsoba);
            ProjektFERContext.Instance.SaveChanges();
        }
    }
}