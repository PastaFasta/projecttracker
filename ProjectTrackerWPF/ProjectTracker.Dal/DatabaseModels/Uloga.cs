﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectTracker.Dal.DatabaseModels
{
    public partial class Uloga
    {
        public Uloga()
        {
            ProjektUlogaOsoba = new HashSet<ProjektUlogaOsoba>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Naziv { get; set; }

        public virtual ICollection<ProjektUlogaOsoba> ProjektUlogaOsoba { get; set; }
    }
}