﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectTracker.Dal.DatabaseModels
{
    public partial class Osoba
    {
        public Osoba()
        {
            ProjektUlogaOsoba = new HashSet<ProjektUlogaOsoba>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Ime { get; set; }
        public string Prezime { get; set; }

        public virtual ICollection<ProjektUlogaOsoba> ProjektUlogaOsoba { get; set; }
    }
}