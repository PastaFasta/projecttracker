﻿using System;
using System.Collections.Generic;

namespace ProjectTracker.Dal.DatabaseModels
{
    public partial class ProjektUlogaOsoba
    {
        public int IdProjekt { get; set; }
        public int IdUloga { get; set; }
        public int IdOsoba { get; set; }

        public virtual Osoba IdOsobaNavigation { get; set; }
        public virtual Projekt IdProjektNavigation { get; set; }
        public virtual Uloga IdUlogaNavigation { get; set; }
    }
}