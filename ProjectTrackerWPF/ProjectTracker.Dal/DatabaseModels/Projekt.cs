﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectTracker.Dal.DatabaseModels
{
    public partial class Projekt
    {
        public Projekt()
        {
            ProjektUlogaOsoba = new HashSet<ProjektUlogaOsoba>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Naziv { get; set; }
        public string Opis { get; set; }
        public DateTime? Zapocet { get; set; }
        public DateTime? Zavrsen { get; set; }

        public virtual ICollection<ProjektUlogaOsoba> ProjektUlogaOsoba { get; set; }
    }
}