﻿using System;
using System.Threading.Tasks;
using Csla;
using Microsoft.AspNetCore.Mvc;

namespace ProjectTracker.AppServerHost.Controllers {
    [Route("/")]
    [ApiController]
    public class DefaultController : Csla.Server.Hosts.HttpPortalController {
        // This override exists for demo purposes only, normally you
        // wouldn't implement this code.


        // Implementing a GET is totally optional, but is nice
        // for quick diagnosis as to whether a service is running.
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<string>> GetAsync() {
            try {
                return $"Epic";
            }
            catch (Exception ex) {
                return ex.ToString();
            }
        }
    }
}