﻿using Csla;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class OsobaUloga : CslaBaseTypes.BusinessBase<OsobaUloga>
    {
        //public static readonly PropertyInfo<int> ProjektIdProperty = RegisterProperty<int>(c => c.ProjektID);

        //public int ProjektID
        //{
        //    get { return GetProperty(ProjektIdProperty); }
        //    set { SetProperty(ProjektIdProperty, value); }
        //}

        public static readonly PropertyInfo<int> OsobaIdProperty = RegisterProperty<int>(c => c.OsobaID);

        public int OsobaID
        {
            get { return GetProperty(OsobaIdProperty); }
            set { SetProperty(OsobaIdProperty, value); }
        }

        public static readonly PropertyInfo<int> UlogaIdProperty = RegisterProperty<int>(c => c.UlogaID);

        public int UlogaID
        {
            get { return GetProperty(UlogaIdProperty); }
            set { SetProperty(UlogaIdProperty, value); }
        }

        public static readonly PropertyInfo<string> PrezimeProperty =
          RegisterProperty<string>(c => c.Prezime);

        [Display(Name = "Prezime")]
        [Required]
        [StringLength(50)]
        public string Prezime
        {
            get { return GetProperty(PrezimeProperty); }
            set { SetProperty(PrezimeProperty, value); }
        }

        public static readonly PropertyInfo<string> ImeProperty =
          RegisterProperty<string>(c => c.Ime);

        [Display(Name = "Ime")]
        [Required]
        [StringLength(50)]
        public string Ime
        {
            get { return GetProperty(ImeProperty); }
            set { SetProperty(ImeProperty, value); }
        }

        [Display(Name = "Puno ime")]
        public string PunoIme
        {
            get { return Prezime + ", " + Ime; }
        }

        public static readonly PropertyInfo<int> UlogaNazivProperty = RegisterProperty<int>(c => c.UlogaNaziv);

        public int UlogaNaziv
        {
            get { return GetProperty(UlogaNazivProperty); }
            set { SetProperty(UlogaNazivProperty, value); }
        }

        #region Factory Methods

        public static OsobaUloga New()
        {
            return DataPortal.Create<OsobaUloga>();
        }

        public static void Delete(int idUloga)
        {
            DataPortal.Delete<Uloga>(idUloga);
        }

        public static Uloga Get(int idUloga)
        {
            return DataPortal.Fetch<Uloga>(idUloga);
        }

        #endregion Factory Methods
    }
}