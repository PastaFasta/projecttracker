﻿using Csla;
using ProjectTracker.Dal;
using System;
using System.Collections.Generic;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class ProjektOsobaUlogaLists : ReadOnlyListBase<ProjektOsobaUlogaLists, ProjektOsobe>
    {
        public ProjektOsobaUlogaLists()
        {
        }

        public static ProjektOsobaUlogaLists Get(int id)
        {
            return DataPortal.Fetch<ProjektOsobaUlogaLists>(id);
        }

        private void DataPortal_Fetch(int id)
        {
            RaiseListChangedEvents = false;
            List<ProjektOsobe> data = new List<ProjektOsobe>();
            List<ProjectTracker.Dal.DatabaseModels.Osoba> osobe = Lookup.GetResursiFromProjekt(id);
            foreach (var osoba in osobe)
            {
                data.Add(new ProjektOsobe(osoba.Id, osoba.Ime, osoba.Prezime));
            }
            IsReadOnly = false;
            this.AddRange(data);
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }
    }
}