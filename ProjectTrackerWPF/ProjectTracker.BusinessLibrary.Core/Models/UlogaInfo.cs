﻿using Csla;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class UlogaInfo : ReadOnlyBase<UlogaInfo>
    {
        public int Id { get; private set; }
        public string Naziv { get; private set; }

        public UlogaInfo()
        {
        }

        internal UlogaInfo(int id, string naziv)
        {
            Id = id;
            Naziv = naziv;
        }

        public override string ToString()
        {
            return Naziv;
        }
    }
}