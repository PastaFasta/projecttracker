﻿using Csla;
using ProjectTracker.Dal;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class Osoba : CslaBaseTypes.BusinessBase<Osoba>
    {
        public static readonly PropertyInfo<int> OsobaIdProperty = RegisterProperty<int>(c => c.OsobaID);

        [Display(Name = "Osoba id")]
        public int OsobaID
        {
            get { return GetProperty(OsobaIdProperty); }
            set { SetProperty(OsobaIdProperty, value); }
        }

        public static readonly PropertyInfo<string> PrezimeProperty =
          RegisterProperty<string>(c => c.Prezime);

        [Display(Name = "Prezime")]
        [Required]
        [StringLength(50)]
        public string Prezime
        {
            get { return GetProperty(PrezimeProperty); }
            set { SetProperty(PrezimeProperty, value); }
        }

        public static readonly PropertyInfo<string> ImeProperty =
          RegisterProperty<string>(c => c.Ime);

        [Display(Name = "Ime")]
        [Required]
        [StringLength(50)]
        public string Ime
        {
            get { return GetProperty(ImeProperty); }
            set { SetProperty(ImeProperty, value); }
        }

        [Display(Name = "Puno ime")]
        public string PunoIme
        {
            get { return Prezime + ", " + Ime; }
        }

        #region Factory Methods

        public static Osoba New()
        {
            return DataPortal.Create<Osoba>();
        }

        public static void Delete(int idOsobe)
        {
            DataPortal.Delete<Osoba>(idOsobe);
        }

        public static Osoba Get(int idOsobe)
        {
            return DataPortal.Fetch<Osoba>(idOsobe);
        }

        public static void Update(Osoba osoba)
        {
            DataPortal.Update<Osoba>(osoba);
        }

        #endregion Factory Methods

        protected override void DataPortal_Create()
        {
            base.DataPortal_Create();
        }

        protected override void DataPortal_Insert()
        {
            Lookup.AddOsoba(Ime, Prezime);
        }

        protected override void DataPortal_Update()
        {
            Lookup.UpdateOsoba(OsobaID, Ime, Prezime);
        }

        protected void DataPortal_Fetch(int id)
        {
            var osoba = Lookup.FetchOsoba(id);

            LoadProperty(OsobaIdProperty, osoba.Id);
            LoadProperty(ImeProperty, osoba.Ime);
            LoadProperty(PrezimeProperty, osoba.Prezime);
        }

        protected void DataPortal_Delete(int id)
        {
            Lookup.RemoveOsoba(id);
        }

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Csla.Rules.CommonRules.Required(ImeProperty));
            BusinessRules.AddRule(new Csla.Rules.CommonRules.Required(PrezimeProperty));
        }
    }
}