﻿using Csla;
using Csla.Rules;
using ProjectTracker.Dal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class Projekt : CslaBaseTypes.BusinessBase<Projekt>
    {
        public Projekt()
        { }

        public static readonly PropertyInfo<int> ProjectIdProperty = RegisterProperty<int>(c => c.ProjektID);

        [Display(Name = "Projekt id")]
        public int ProjektID
        {
            get { return ReadProperty(ProjectIdProperty); }
            set { LoadProperty(ProjectIdProperty, value); }
        }

        public static readonly PropertyInfo<string> ProjektNazivProperty = RegisterProperty<string>(c => c.Naziv);

        [Display(Name = "Naziv")]
        public string Naziv
        {
            get { return ReadProperty(ProjektNazivProperty); }
            set { LoadProperty(ProjektNazivProperty, value); }
        }

        public static readonly PropertyInfo<string> ProjektOpisProperty = RegisterProperty<string>(c => c.Opis);

        [Display(Name = "Opis")]
        public string Opis
        {
            get { return ReadProperty(ProjektOpisProperty); }
            set { LoadProperty(ProjektOpisProperty, value); }
        }

        public static readonly PropertyInfo<DateTime?> ProjektPocetakProperty = RegisterProperty<DateTime?>(c => c.DatPocetak);

        [Display(Name = "Datum Pocetka")]
        public DateTime? DatPocetak
        {
            get { return ReadProperty(ProjektPocetakProperty); }
            set { LoadProperty(ProjektPocetakProperty, value); }
        }

        public static readonly PropertyInfo<DateTime?> ProjektZavrsetakProperty = RegisterProperty<DateTime?>(c => c.DatZavrsetak);

        [Display(Name = "Datum Zavrsetka")]
        public DateTime? DatZavrsetak
        {
            get { return ReadProperty(ProjektPocetakProperty); }
            set { LoadProperty(ProjektPocetakProperty, value); }
        }

        public static readonly PropertyInfo<ObservableCollection<OsobaUloga>> ProjektOsobeProperty = RegisterProperty<ObservableCollection<OsobaUloga>>(c => c.Osobe);

        [Display(Name = "Osobe")]
        public ObservableCollection<OsobaUloga> Osobe
        {
            get { return ReadProperty(ProjektOsobeProperty); }
            private set { LoadProperty(ProjektOsobeProperty, value); }
        }

        #region Factory Methods

        public static Projekt New()
        {
            return DataPortal.Create<Projekt>();
        }

        public static void Delete(int idProjekta)
        {
            DataPortal.Delete<Projekt>(idProjekta);
        }

        public static Projekt Get(int idProjekta)
        {
            return DataPortal.Fetch<Projekt>(idProjekta);
        }
        public static void Update(Projekt projekt)
        {
            DataPortal.Update<Projekt>(projekt);
        }

        #endregion Factory Methods

        protected override void DataPortal_Create()
        {
            base.DataPortal_Create();
        }

        protected override void DataPortal_Insert()
        {
            Lookup.AddProjekt(Naziv, Opis, DatPocetak, DatZavrsetak);
        }
        protected override void DataPortal_Update()
        {
            Lookup.UpdateProjekt(ProjektID, Naziv, Opis, DatPocetak, DatZavrsetak);
        }

        protected void DataPortal_Fetch(int id)
        {
            var projekt = Lookup.FetchProjekt(id);
            List<ProjectTracker.Dal.DatabaseModels.Osoba> osobe = Lookup.GetResursiFromProjekt(id);
            LoadProperty(ProjectIdProperty, projekt.Id);
            LoadProperty(ProjektNazivProperty, projekt.Naziv);
            LoadProperty(ProjektOpisProperty, projekt.Opis);
            LoadProperty(ProjektPocetakProperty, projekt.Zapocet);
            LoadProperty(ProjektZavrsetakProperty, projekt.Zavrsen);
        }

        protected void DataPortal_Delete(int id)
        {
            Lookup.RemoveProjekt(id);
        }

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Csla.Rules.CommonRules.MaxLength(ProjektNazivProperty, 50));
            BusinessRules.AddRule(new DatumStartVeciOdKrajRule
            {
                PrimaryProperty = ProjektPocetakProperty,
                AffectedProperties = { ProjektZavrsetakProperty }
            });
            BusinessRules.AddRule(
              new DatumStartVeciOdKrajRule
              {
                  PrimaryProperty = ProjektZavrsetakProperty,
                  AffectedProperties = { ProjektPocetakProperty }
              });
            BusinessRules.AddRule(new NoDuplicateResource
            {
                PrimaryProperty = ProjektPocetakProperty,
            });
        }

        private class DatumStartVeciOdKrajRule : Csla.Rules.BusinessRule
        {
            protected override void Execute(IRuleContext context)
            {
                Projekt target = (Projekt)context.Target;

                var started = target.ReadProperty(ProjektPocetakProperty);
                var ended = target.ReadProperty(ProjektZavrsetakProperty);
                if (started.HasValue && ended.HasValue && started > ended || !started.HasValue && ended.HasValue)
                    context.AddErrorResult("Datum pocetka ne smije biti nakon datuma kraja");
            }
        }

        private class NoDuplicateResource : Csla.Rules.BusinessRule
        {
            protected override void Execute(Csla.Rules.IRuleContext context)
            {
                Projekt target = (Projekt)context.Target;
                foreach (var item in target.Osobe)
                {
                    var count = target.Osobe.Count(r => r.OsobaID == item.OsobaID);
                    if (count > 1)
                    {
                        context.AddErrorResult("Dodavanje vec dodane osobe");
                        return;
                    }
                }
            }
        }
    }
}