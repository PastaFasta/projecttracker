﻿using Csla;
using System;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class OsobaInfo : ReadOnlyBase<OsobaInfo>
    {
        public int Id { get; private set; }
        public string Ime { get; private set; }
        public string Prezime { get; private set; }
        public string PunoIme { get { return Prezime + ", " + Ime; } }

        public OsobaInfo()
        {
        }

        internal OsobaInfo(int id, string ime, string prezime)
        {
            Id = id;
            Ime = ime;
            Prezime = prezime;
        }

        public override string ToString()
        {
            return PunoIme;
        }
    }
}