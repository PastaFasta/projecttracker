﻿using Csla;
using ProjectTracker.Dal;
using System;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class Uloga : CslaBaseTypes.BusinessBase<Uloga>
    {
        public static readonly PropertyInfo<int> UlogaIdProperty = RegisterProperty<int>(c => c.UlogaID);

        public int UlogaID
        {
            get { return ReadProperty(UlogaIdProperty); }
            set { LoadProperty(UlogaIdProperty, value); }
        }

        public static readonly PropertyInfo<string> UlogaNazivProperty = RegisterProperty<string>(c => c.Naziv);

        public string Naziv
        {
            get { return ReadProperty(UlogaNazivProperty); }
            set { LoadProperty(UlogaNazivProperty, value); }
        }

        #region Factory Methods

        public static Uloga New()
        {
            return DataPortal.Create<Uloga>();
        }

        public static void Delete(int idUloga)
        {
            DataPortal.Delete<Uloga>(idUloga);
        }

        public static Uloga Get(int idUloga)
        {
            return DataPortal.Fetch<Uloga>(idUloga);
        }

        public static void Update(Uloga uloga)
        {
            DataPortal.Update<Uloga>(uloga);
        }

        #endregion Factory Methods

        protected override void DataPortal_Create()
        {
            base.DataPortal_Create();
        }

        protected override void DataPortal_Insert()
        {
            Lookup.AddUloga(Naziv);
        }

        protected void DataPortal_Fetch(int id)
        {
            ProjectTracker.Dal.DatabaseModels.Uloga uloga = Lookup.FetchUloga(id);

            LoadProperty(UlogaIdProperty, uloga.Id);
            LoadProperty(UlogaNazivProperty, uloga.Naziv);
            BusinessRules.CheckRules();
        }

        protected void DataPortal_Delete(int id)
        {
            Lookup.RemoveUloga(id);
        }

        protected override void DataPortal_Update()
        {
            Lookup.UpdateUloga(UlogaID, Naziv);
        }

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            //BusinessRules.AddRule(new Csla.Rules.CommonRules.Required(UlogaNazivProperty));
            BusinessRules.AddRule(new Csla.Rules.CommonRules.MaxLength(UlogaNazivProperty, 50));
            BusinessRules.AddRule(new Csla.Rules.CommonRules.MinLength(UlogaNazivProperty, 2));
            //BusinessRules.AddRule(new Csla.Rules.CommonRules.Required(UlogaIdProperty));
        }
    }
}