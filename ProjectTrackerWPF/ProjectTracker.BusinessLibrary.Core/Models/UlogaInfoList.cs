﻿using Csla;
using ProjectTracker.Dal;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class UlogaInfoList : ReadOnlyListBase<UlogaInfoList, UlogaInfo>
    {
        public UlogaInfoList()
        {
        }

        public static UlogaInfoList Get()
        {
            return DataPortal.Fetch<UlogaInfoList>();
        }

        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;
            List<UlogaInfo> data = new List<UlogaInfo>();
            List<ProjectTracker.Dal.DatabaseModels.Uloga> uloge = Lookup.FetchUloge();
            foreach (var uloga in uloge)
            {
                data.Add(new UlogaInfo(uloga.Id, uloga.Naziv));
            }
            IsReadOnly = false;
            this.AddRange(data);
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }
    }
}