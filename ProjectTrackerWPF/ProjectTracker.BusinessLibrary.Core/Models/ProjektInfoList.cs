﻿using Csla;
using ProjectTracker.Dal;
using System;
using System.Collections.Generic;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class ProjektInfoList : ReadOnlyListBase<ProjektInfoList, ProjektInfo>
    {
        public ProjektInfoList()
        {
        }

        public static ProjektInfoList Get()
        {
            return DataPortal.Fetch<ProjektInfoList>();
        }

        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;
            List<ProjektInfo> data = new List<ProjektInfo>();
            List<ProjectTracker.Dal.DatabaseModels.Projekt> projekti = Lookup.FetchProjekte();
            foreach (var projekt in projekti)
            {
                data.Add(new ProjektInfo(projekt.Id, projekt.Naziv, projekt.Opis, projekt.Zapocet, projekt.Zavrsen));
            }
            IsReadOnly = false;
            this.AddRange(data);
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }
    }
}