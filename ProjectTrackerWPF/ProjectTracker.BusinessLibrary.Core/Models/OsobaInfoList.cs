﻿using Csla;
using ProjectTracker.Dal;
using System;
using System.Collections.Generic;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class OsobaInfoList : ReadOnlyListBase<OsobaInfoList, OsobaInfo>
    {
        public OsobaInfoList()
        {
        }

        public static OsobaInfoList Get()
        {
            return DataPortal.Fetch<OsobaInfoList>();
        }

        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;
            List<OsobaInfo> data = new List<OsobaInfo>();
            List<ProjectTracker.Dal.DatabaseModels.Osoba> osobe = Lookup.FetchOsobe();
            foreach (var osoba in osobe)
            {
                data.Add(new OsobaInfo(osoba.Id, osoba.Ime, osoba.Prezime));
            }
            IsReadOnly = false;
            this.AddRange(data);
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }
    }
}