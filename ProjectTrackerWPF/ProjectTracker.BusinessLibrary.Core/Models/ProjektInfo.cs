﻿using Csla;
using System;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class ProjektInfo : ReadOnlyBase<ProjektInfo>
    {
        public int Id { get; private set; }
        public string Naziv { get; private set; }
        public string Opis { get; private set; }
        public DateTime? Zapocet { get; private set; }
        public DateTime? Zavrsen { get; private set; }

        public ProjektInfo()
        {
        }

        internal ProjektInfo(int id, string naziv, string opis, DateTime? zapocet, DateTime? zavrsen)
        {
            Id = id;
            Naziv = naziv;
            Opis = opis;
            Zapocet = zapocet;
            Zavrsen = zavrsen;
        }

        public override string ToString()
        {
            return Naziv;
        }
    }
}