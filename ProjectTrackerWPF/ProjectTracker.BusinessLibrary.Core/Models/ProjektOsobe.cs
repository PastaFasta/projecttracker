﻿using Csla;
using System;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class ProjektOsobe : ReadOnlyBase<ProjektOsobe>
    {
        public int ProjektId { get; set; }
        public int OsobaId { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }

        public string PunoIme { get { return Prezime + ", " + Ime; } }

        public ProjektOsobe()
        {
        }

        public ProjektOsobe(int osobaId, string ime, string prezime)
        {
            OsobaId = osobaId;
            Ime = ime;
            Prezime = prezime;
        }
    }
}