﻿using Csla;
using System;

namespace ProjectTracker.BusinessLibrary.Core.Models
{
    [Serializable]
    public class ProjektOsobaUloga : CslaBaseTypes.BusinessBase<ProjektOsobaUloga>
    {
        public static readonly PropertyInfo<int> OsobaIdProperty = RegisterProperty<int>(c => c.OsobaID);

        public int OsobaID
        {
            get { return GetProperty(OsobaIdProperty); }
            set { SetProperty(OsobaIdProperty, value); }
        }

        public static readonly PropertyInfo<int> UlogaIdProperty = RegisterProperty<int>(c => c.UlogaID);

        public int UlogaID
        {
            get { return GetProperty(UlogaIdProperty); }
            set { SetProperty(UlogaIdProperty, value); }
        }

        public static readonly PropertyInfo<string> PrezimeProperty = RegisterProperty<string>(c => c.Prezime);

        public string Prezime
        {
            get { return GetProperty(PrezimeProperty); }
            set { SetProperty(PrezimeProperty, value); }
        }

        public static readonly PropertyInfo<string> ImeProperty = RegisterProperty<string>(c => c.Ime);

        public string Ime
        {
            get { return GetProperty(ImeProperty); }
            set { SetProperty(ImeProperty, value); }
        }

        public static readonly PropertyInfo<string> UlogaNazivProperty = RegisterProperty<string>(c => c.UlogaNaziv);

        public string UlogaNaziv
        {
            get { return GetProperty(UlogaNazivProperty); }
            set { SetProperty(UlogaNazivProperty, value); }
        }

        public string PunoIme
        {
            get { return Prezime + ", " + Ime; }
        }

        #region Factory Methods

        public static ProjektOsobaUloga New()
        {
            return DataPortal.Create<ProjektOsobaUloga>();
        }

        public static void Delete(int idUloga)
        {
            DataPortal.Delete<Uloga>(idUloga);
        }

        public static Uloga Get(int idUloga)
        {
            return DataPortal.Fetch<Uloga>(idUloga);
        }

        #endregion Factory Methods
    }
}