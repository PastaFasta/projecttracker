﻿using System;

namespace ProjectTracker.BusinessLibrary.Core.CslaBaseTypes
{
    [Serializable]
    public abstract class BusinessBase<T> : Csla.BusinessBase<T> where T : Csla.BusinessBase<T>
    {
    }
}