﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.PlatformUI;
using ProjectTracker.BusinessLibrary.Core.Models;
namespace ProjectTrackerMVC.Controllers
{
    public class UlogasController : Controller
    {
        private bool _novi;
        private ICommand _dodajUloguCommand;
        private ICommand _urediUloguCommand;
        private ICommand _spremiUloguCommand;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyRaised(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }
        public ICommand UrediUloguCommand
        {
            get { return _urediUloguCommand ?? (_urediUloguCommand = new DelegateCommand(UrediUlogu)); }
        }

        public ICommand NovaUlogaCommand
        {
            get { return _dodajUloguCommand ?? (_dodajUloguCommand = new DelegateCommand(DodajUlogu)); }
        }

        public ICommand SpremiUloguCommand
        {
            get { return _spremiUloguCommand ?? (_spremiUloguCommand = new DelegateCommand(SpremiUlogu)); }
        }
        public UlogaInfoList ListaUloga { get; set; } = UlogaInfoList.Get();
        // GET: Ulogas
        public async Task<IActionResult> Index()
        {
            return View(ListaUloga);
        }
        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv")]Uloga uloga)
        {
            Uloga ulogaa;
            ulogaa = Uloga.New();
            ulogaa.Naziv = uloga.Naziv;
            Uloga u = ulogaa.Save();
            ListaUloga = UlogaInfoList.Get();
            return RedirectToAction(nameof(Index));
        }
        public string Naziv { get; set; } = String.Empty;
        public bool MozeSpremit { get; set; } = false;
        public bool IsReadUloga { get; set; } = true;

        public UlogaInfo SelektiranaUloga { get; set; }
        private void UrediUlogu()
        {
            _novi = false;
            Naziv = SelektiranaUloga.Naziv;
            MozeSpremit = true;
            IsReadUloga = false;
            OnPropertyRaised("Naziv");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadUloga");
        }

        private void DodajUlogu()
        {
            _novi = true;
            Naziv = String.Empty;
            MozeSpremit = true;
            IsReadUloga = false;
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadUloga");
        }

        private void SpremiUlogu()
        {
            Uloga uloga;
            if (_novi)
            {
                uloga = Uloga.New();
                uloga.Naziv = Naziv;
                Uloga u = uloga.Save();
            }
            else
            {
                uloga = Uloga.Get(SelektiranaUloga.Id);
                uloga.Naziv = Naziv;
                Uloga.Update(uloga);
            }

            MozeSpremit = false;
            IsReadUloga = true;
            OnPropertyRaised("Naziv");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadUloga");
            var bu = uloga.GetBrokenRules();
            Naziv = String.Empty;
            ListaUloga = UlogaInfoList.Get();
            OnPropertyRaised("ListaUloga");
        }
    

}       
}
