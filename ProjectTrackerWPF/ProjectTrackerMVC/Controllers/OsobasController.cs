﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.PlatformUI;
using ProjectTracker.BusinessLibrary.Core.Models;
using ProjectTracker.Dal;

namespace ProjectTrackerMVC.Controllers
{
    public class OsobasController : Controller
    {
        private bool _novi;
        private string _ime;
        private string _prezime;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyRaised(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }

        private ICommand _newOsobaCommand;
        private ICommand _editOsobaCommand;
        private ICommand _saveOsobaCommand;
        public async Task<IActionResult> Index()
        {
            return View(ListaOsoba);
        }
        public async Task<IActionResult> Create()
        {
            return View();
        }
        public async Task<IActionResult> Edit()
        {
            Osoba osobaa;
            osobaa = Osoba.New();
            osobaa.Ime = "Luka";
            osobaa.Prezime = "Peric";
            return View(osobaa);
        }
        public async Task<IActionResult> Delete()
        {
            Osoba osobaa;
            osobaa = Osoba.New();
            osobaa.Ime = "Luka";
            osobaa.Prezime = "Peric";
            return View(osobaa);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Ime,Prezime")]Osoba osoba)
        {
            Osoba osobaa;
            osobaa = Osoba.New();
            osobaa.Ime = osoba.Ime;
            osobaa.Prezime = osoba.Prezime;
            Osoba o = osobaa.Save();
            ListaOsoba = OsobaInfoList.Get();
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var osoba = Lookup.FetchOsoba(id);
            if (osoba == null)
            {
                return NotFound();
            }

            return View(osoba);
        }

        public ICommand NovaOsobaCommand
        {
            get { return _newOsobaCommand ?? (_newOsobaCommand = new DelegateCommand(DodajOsobu)); }
        }

        public ICommand EditOsobaCommand
        {
            get { return _editOsobaCommand ?? (_editOsobaCommand = new DelegateCommand(UrediOsobu)); }
        }

        public ICommand SpremiOsobuCommand
        {
            get { return _saveOsobaCommand ?? (_saveOsobaCommand = new DelegateCommand(SpremiOsobu)); }
        }

        public string Ime
        {
            get { return _ime; }
            set { _ime = value; }
        }

        public string Prezime
        {
            get { return _prezime; }
            set { _prezime = value; }
        }

        public string PunoIme
        {
            get { return Ime + " " + Prezime; }
        }

        public bool MozeSpremit { get; set; } = false;
        public bool IsReadOsoba { get; set; } = true;

        public OsobaInfoList ListaOsoba { get; set; } = OsobaInfoList.Get();
        List<ProjectTracker.Dal.DatabaseModels.Osoba> osobe { get; set; } = Lookup.FetchOsobe();

        public OsobaInfo SelektiranaOsoba { get; set; }
        private void DodajOsobu()
        {
            _novi = true;
            Ime = String.Empty;
            Prezime = String.Empty;
            IsReadOsoba = false;
            MozeSpremit = true;
            OnPropertyRaised("Ime");
            OnPropertyRaised("Prezime");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadOsoba");
        }

        private void UrediOsobu()
        {
            _novi = false;
            Ime = SelektiranaOsoba.Ime;
            Prezime = SelektiranaOsoba.Prezime;
            IsReadOsoba = false;
            MozeSpremit = true;
            OnPropertyRaised("Ime");
            OnPropertyRaised("Prezime");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadOsoba");
        }

        private void SpremiOsobu()
        {
            Osoba osoba;
            if (_novi)
            {
                osoba = Osoba.New();
                osoba.Ime = Ime;
                osoba.Prezime = Prezime;
                ProjectTracker.BusinessLibrary.Core.Models.Osoba o = osoba.Save();
            }
            else
            {
                osoba = Osoba.Get(SelektiranaOsoba.Id);
                osoba.Ime = Ime;
                osoba.Prezime = Prezime;
                Osoba.Update(osoba);
            }
            MozeSpremit = false;
            IsReadOsoba = true;
            OnPropertyRaised("Ime");
            OnPropertyRaised("Prezime");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadOsoba");
            Ime = Prezime = String.Empty;
            ListaOsoba = OsobaInfoList.Get();
            OnPropertyRaised("ListaOsoba");
        }


    }
}
