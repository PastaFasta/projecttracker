﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectTracker.BusinessLibrary.Core.Models;

namespace ProjectTrackerMVC.Controllers
{
    public class ProjektsController : Controller
    {
        private readonly ProjectTracker.Dal.ProjektFERContext _context;
        private bool _novi;
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public DateTime? Zapocet { get; set; }
        public DateTime? Zavrsen { get; set; }
        public ProjektInfoList ListaProjekata { get; set; } = ProjektInfoList.Get();
        List<ProjectTracker.Dal.DatabaseModels.Projekt> projekti { get; set; } = ProjectTracker.Dal.Lookup.FetchProjekte();

        // GET: Projekts
        public async Task<IActionResult> Index()
        {
            return View(ListaProjekata);
        }

        // GET: Projekts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            foreach (var item in ListaProjekata)
            {
                if(item.Id == id)
                {
                    Naziv = item.Naziv;
                    Opis = item.Opis;
                    Zapocet = item.Zapocet;
                    Zavrsen = item.Zavrsen;
                }
            }

            return View();
        }

        // GET: Projekts/Create
        public IActionResult Create()
        {
            return View();
        }
       
        // POST: Projekts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,Opis,Zapocet,Zavrsen")] Projekt projekt)
        {
            if (ModelState.IsValid)
            {
                _context.Add(projekt);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(projekt);
        }

       

        // POST: Projekts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
       
    }
}
