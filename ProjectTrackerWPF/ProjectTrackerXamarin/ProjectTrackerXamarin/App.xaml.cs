﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectTrackerXamarin {
    public partial class App : Application {
        public static Page RootPage { get; private set; }
        public App() {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
            RootPage = MainPage;
        }

        protected override void OnStart() {
            // Handle when your app starts
            Csla.ApplicationContext.DataPortalProxy = typeof(Csla.DataPortalClient.HttpProxy).AssemblyQualifiedName;
            Csla.ApplicationContext.DataPortalUrlString = "https://projektzafer2019.azurewebsites.net/";
        }

        protected override void OnSleep() {
            // Handle when your app sleeps
        }

        protected override void OnResume() {
            // Handle when your app resumes
        }
        public static async Task NavigateTo(Type page) {
            var target = (Page)Activator.CreateInstance(page);
            await NavigateTo(target);
        }

        public static async Task NavigateTo(Type page, object parameter) {
            var target = (Page)Activator.CreateInstance(page, parameter);
            await NavigateTo(target);
        }

        private static async Task NavigateTo(Page page) {
            await Csla.Reflection.MethodCaller.CallMethodTryAsync(page, "InitAsync");
            await RootPage.Navigation.PushAsync(page);
        }
    }
}
