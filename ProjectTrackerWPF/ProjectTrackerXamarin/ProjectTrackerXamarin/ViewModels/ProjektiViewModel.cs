﻿using ProjectTracker.BusinessLibrary.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjectTrackerXamarin.ViewModels
{
    class ProjektiViewModel : INotifyPropertyChanged
    {
        private bool _novi;
        private string _naziv;
        private string _opis;
        private DateTime? _zapocet;
        private DateTime? _zavrsen;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyRaised(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }
        public string Naziv
        {
            get { return _naziv; }
            set { _naziv = value; }
        }
        public string Opis
        {
            get { return _opis; }
            set { _opis = value; }
        }
        public DateTime? Zapocet
        {
            get { return _zapocet; }
            set { _zapocet = value; }
        }
        public DateTime? Zavrsen
        {
            get { return _zavrsen; }
            set { _zavrsen = value; }
        }
        public ICommand NewProjektCommand { get; private set; }
        public ICommand SaveProjektCommand { get; private set; }



        public bool MozeSpremit { get; set; } = false;
        public bool IsReadProjekt { get; set; } = false;

        public ProjektInfoList ListaProjekta { get; set; } = ProjektInfoList.Get();

        public ProjektInfo SelektiranProjekt { get; set; }

        public ProjektiViewModel()
        {
            NewProjektCommand = new Command(() => {
                DodajProjekt();
            });
            SaveProjektCommand = new Command(() => {
                SpremiProjekt();
            });
        }

        private void DodajProjekt()
        {
            _novi = true;
            Naziv = String.Empty;
            Opis = String.Empty;
            Zapocet = DateTime.Today;
            Zavrsen = DateTime.Today;
            IsReadProjekt = true;
            MozeSpremit = true;
            OnPropertyRaised("Naziv");
            OnPropertyRaised("Opis");
            OnPropertyRaised("Zapocet");
            OnPropertyRaised("Zavrsen");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadProjekt");
        }
        private void UrediProjekt()
        {
            _novi = false;
            Naziv = SelektiranProjekt.Naziv;
            Opis = SelektiranProjekt.Opis;
            Zapocet = SelektiranProjekt.Zapocet;
            Zavrsen = SelektiranProjekt.Zavrsen;
            IsReadProjekt = false;
            MozeSpremit = true;
            OnPropertyRaised("Naziv");
            OnPropertyRaised("Opis");
            OnPropertyRaised("Zapocet");
            OnPropertyRaised("Zavrsen");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadProjekt");
        }
        private void SpremiProjekt()
        {
            Projekt projekt;
            if (_novi)
            {
                projekt = Projekt.New();
                projekt.Naziv = Naziv;
                projekt.Opis = Opis;
                projekt.DatPocetak = Zapocet;
                projekt.DatZavrsetak = Zavrsen;
                Projekt p = projekt.Save();
            }
            else
            {
                projekt = Projekt.Get(SelektiranProjekt.Id);
                projekt.Naziv = Naziv;
                projekt.Opis = Opis;
                projekt.DatPocetak = Zapocet;
                projekt.DatZavrsetak = Zavrsen;
                Projekt.Update(projekt);
            }
            IsReadProjekt = true;
            MozeSpremit = false;
            OnPropertyRaised("Naziv");
            OnPropertyRaised("Opis");
            OnPropertyRaised("Zapocet");
            OnPropertyRaised("Zavrsen");
            OnPropertyRaised("MozeSpremit");
            OnPropertyRaised("IsReadProjekt");
            Naziv = String.Empty;
            Opis = String.Empty;
            Zapocet = DateTime.Today;
            Zavrsen = DateTime.Today;
            ListaProjekta = ProjektInfoList.Get();
            OnPropertyRaised("ListaProjekta");
        }
    }
}
