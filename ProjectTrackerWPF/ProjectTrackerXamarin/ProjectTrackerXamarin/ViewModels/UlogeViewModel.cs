﻿using ProjectTracker.BusinessLibrary.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ProjectTrackerXamarin.ViewModels
{
    public class UlogeViewModel : INotifyPropertyChanged
    {
        public string Naziv { get; set; } = String.Empty;
        public bool MozeSpremit { get; set; } = false;
        public bool IsReadUloga { get; set; } = true;
        public UlogaInfoList ListaUloga { get; set; } = UlogaInfoList.Get();

        public UlogaInfo SelektiranaUloga { get; set; }

        public UlogeViewModel()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
