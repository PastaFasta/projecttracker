﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectTrackerXamarin {
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage {
        public MainPage() {
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, EventArgs e) {
            await App.NavigateTo(typeof(Views.OsobeView));
        }

        private async void PokaziProjekte(object sender, EventArgs e)
        {
            await App.NavigateTo(typeof(Views.ProjektiView));
        }

        private async void PokaziOsobe(object sender, EventArgs e)
        {
            await App.NavigateTo(typeof(Views.OsobeView));
        }

        private async void PokaziUloge(object sender, EventArgs e)
        {
            await App.NavigateTo(typeof(Views.UlogeView));
        }



    }
}
