﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectTrackerXamarin.ViewModels;

namespace ProjectTrackerXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OsobeView : ContentPage
    {
        OsobeViewModel Model;
        public OsobeView()
        {
            InitializeComponent();
            BindingContext = new OsobeViewModel();
            Model = new OsobeViewModel();
        }
        public async Task InitAsync()
        {
            BindingContext = new OsobeViewModel();
        }

    }
}
