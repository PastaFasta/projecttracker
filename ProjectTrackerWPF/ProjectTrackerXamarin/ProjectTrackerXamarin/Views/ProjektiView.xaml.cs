﻿using ProjectTrackerXamarin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectTrackerXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProjektiView : ContentPage
    {
        public ProjektiView()
        {
            InitializeComponent();
            BindingContext = new ProjektiViewModel();
        }
        public async Task InitAsync()
        {
            BindingContext = new ProjektiViewModel();
        }
    }
}