
CREATE TABLE Projekt(
	ID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Naziv VARCHAR(100),
	Opis VARCHAR(100),
	Zapocet DATETIME,
	Zavrsen DATETIME);

CREATE TABLE Uloga(
	ID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Naziv VARCHAR(100)
	);

CREATE TABLE Osoba(
	ID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Ime VARCHAR(20),
	Prezime VARCHAR(50)
	);

CREATE TABLE ProjektUlogaOsoba(
	ID_Projekt INT NOT NULL REFERENCES Projekt (ID),
	ID_Uloga INT NOT NULL REFERENCES Uloga (ID),
	ID_Osoba INT NOT NULL REFERENCES Osoba (ID),
	PRIMARY KEY (ID_Projekt, ID_Osoba)
	);


	set identity_insert dbo.Uloga on;

INSERT INTO Osoba VALUES ('Ivan', 'Srbljinovic');
INSERT INTO Osoba VALUES ('Pero', 'Peric');
INSERT INTO Osoba VALUES ('Vajdo', 'Wajdovich');

INSERT INTO Uloga values('Projektni mena�er');
INSERT INTO Uloga values('Developer');
INSERT INTO Uloga values('QA');
INSERT INTO Uloga values('Sponza');

INSERT INTO Projekt values('Projekt 2019/2020', 'Zavrsni projekt', null, null);

INSERT INTO ProjektUlogaOsoba values(1, 1, 1);

SELECT * FROM Projekt;
SELECT * FROM Osoba;
SELECT * FROM Uloga;
SELECT * FROM ProjektUlogaOsoba;